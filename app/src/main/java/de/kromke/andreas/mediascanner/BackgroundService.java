/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.os.Binder;
import android.os.Environment;
import android.util.Log;
import android.app.Service;
import android.os.IBinder;
import android.content.Intent;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import static android.os.Environment.getExternalStorageState;

/** @noinspection JavadocLinkAsPlainText, JavadocBlankLines, RedundantSuppression , CommentedOutCode */
public class BackgroundService extends Service
{
    private static final String LOG_TAG = "CMS : BackgroundService";
    private String mDbPath;
    private final IBinder mMediaScannerBind = new MediaScannerServiceBinder();

    /**************************************************************************
     *
     * binder
     *
     *************************************************************************/
    private static class MediaScannerServiceBinder extends Binder
    {
    }

/*
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
*/

    /**************************************************************************
     *
     * Service function: will bind to service
     *
     * return null in case we do not support binding
     *
     *************************************************************************/
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d(LOG_TAG, "onBind()");
        UserSettings.setContext(this);
        evalIntent(intent);
        return mMediaScannerBind;
    }


    /**************************************************************************
     *
     * Service function: release resources
     *
     *************************************************************************/
    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.d(LOG_TAG, "onUnbind()");
        return super.onUnbind(intent);
    }


    @Override
    public void onCreate()
    {
        //Toast.makeText(this, "onCreate()", Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "onCreate()");
    }

    @Override
    public void onDestroy()
    {
        //Toast.makeText(this, "onDestroy()", Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "onDestroy()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //Toast.makeText(this, "onStartCommand()", Toast.LENGTH_LONG).show();
        Log.d(LOG_TAG, "onStartCommand()");
        UserSettings.setContext(this);
        evalIntent(intent);
        return START_NOT_STICKY;
    }


    /************************************************************************************
     *
     * get base path from environment or from settings
     *
     ***********************************************************************************/
    private String getSharedDbBasePath()
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStorageDirectory();
        } else
        {
            basePath = getFilesDir();
        }

        return basePath.getPath() + "/" + UserSettings.dbDirectory;
    }


    /************************************************************************************
     *
     * evaluate intent
     *
     ***********************************************************************************/
    protected void evalIntent(Intent intent)
    {
        Log.d(LOG_TAG, "evalIntent()");
        ArrayList<String> audioPathArrayList = intent.getStringArrayListExtra("pathTable");
        if (audioPathArrayList != null)
        {
            mDbPath = getSharedDbBasePath();
            File dir = new File(mDbPath);
            if (!dir.isDirectory() && !dir.mkdirs())
            {
                Log.e(LOG_TAG, "getMusicBasePath(): cannot create directories");
            } else
            {
                Toast.makeText(this, R.string.str_updating_db, Toast.LENGTH_LONG).show();
                updateFiles(audioPathArrayList);
            }
        }
        else
        {
            Log.d(LOG_TAG, "no paths passed");
        }
    }


    /************************************************************************************
     *
     * evaluate intent
     *
     ***********************************************************************************/
    protected void updateFiles(ArrayList<String> audioPathArrayList)
    {
        Log.d(LOG_TAG, "updateFiles()");
        // concat paths with LF
        int size = audioPathArrayList.size();
        int i;
        StringBuilder sb = new StringBuilder(1024);
        for (i = 0; i < size; i++)
        {
            sb.append(audioPathArrayList.get(i));
            if (i < size - 1)
            {
                sb.append('\n');
            }
        }

        String dbfile = mDbPath + "/" + MainActivity.dbfilename;
        ScannerManager scanner = new ScannerManager();
        String albumpicfname = UserSettings.getAlbumPicFname();
        int flags = UserSettings.getBool(UserSettings.PREF_IGNORE_NOMEDIA, false) ? 0 : 1;
        scanner.scanDirectoryJNI(ScannerManager.cmdJniScanUpdateSpecifiedAudioFiles, dbfile, sb.toString(), albumpicfname,flags);
    }
}
