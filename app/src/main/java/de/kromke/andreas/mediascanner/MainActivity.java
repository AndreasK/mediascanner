/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;


/** @noinspection JavadocLinkAsPlainText, JavadocBlankLines, RedundantSuppression , DanglingJavadoc , CommentedOutCode */
@SuppressWarnings("Convert2Lambda")
public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "CMS : MainActivity";
    static final String dbfilename = "musicmetadata.db";
    //protected static final int RESULT_SETTINGS = 1;
    private final static int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE = 11;
    private final static int MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES = 12;
    boolean mFileReadPermissionGranted = false;
    boolean mFileWritePermissionGranted = false;
    ActivityResultLauncher<Intent> mPreferencesActivityLauncher;
    ActivityResultLauncher<Intent> mStorageAccessPermissionActivityLauncher;
    boolean mbAsyncTaskBusy = false;
    private boolean mbShowDebugMenuEntries = false;
    private enum actionType
    {
        actionRebuildAll,
        actionScanDirectories,
        actionCollectMedia,
        actionQueryAudioFiles,
        actionQueryAlbums,
        actionQueryAlbumPictures,
        actionBuildAlbums,
        actionTruncateAudioFileTable,
        actionTruncateAlbumTable,
        actionRemoveTables,
        actionGetNumbers,
        actionUpdateFiles,
        actionAutoScan,
        actionWriteDbVersion
    }

    // message request from Classical Music Tagger to update single audio files
    private static final String stateParamPathTable = "pathTable";
    // Message request from Music Player to create or update a database for a given path.
    // The database is located in path/ClassicalMusicDb
    private static final String stateParamMusicAndDbPath = "musicPath";
    private static final String stateSavedStatusText = "currText";

    protected MenuItem mMenuItemManageFiles;
    FloatingActionButton mFloatingButton1;
    FloatingActionButton mFloatingButton2;
    TextView mText;
    ProgressBar mProgressBar;
    Timer mTimer;
    MyTimerTask mTimerTask;
    private static final int timerFrequency = 300;     // milliseconds
    int mNumAudioFiles;        // for progress calculation
    int mCallbackArgAudioFilesProcessed;
    int mCallbackArgDirectories;
    int mCallbackArgAlbums;
    int mCallbackArgPicturesExtracted;
    int mCallbackArgPicturesExtractionFailed;
    int mCallbackArgFolderPicturesScaled;
    int mCallbackArgFolderPicturesRenamed;
    int mCallbackArgFolderPicturesFound;

    int mCallbackArgAudioFilesProcessedFinal;
    String mStartStatusText;
    String mStatusText;
    String mFinalStatusText = "";
    private String mPrefMusicBasePath;
    private String mPrefDataBasePath;
    private boolean mPrefDbgRequestMediaFileAccess;


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /* don't remember why this was here...
        //HACK
        Intent intent = getIntent();
        // message from Music Tagger, holding table of audio files that have been changed
        mAudioPathArrayList = intent.getStringArrayListExtra(stateParamPathTable);
        */

        super.onCreate(savedInstanceState);
        UserSettings.setContext(this);
        readFileSystemRelatedPreferences();
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     // removed to allow landscape
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        registerPreferencesCallback();
        registerStorageAccessPermissionCallback();

        mFloatingButton1 = findViewById(R.id.fab);
        mFloatingButton2 = findViewById(R.id.fab2);
        mText = findViewById(R.id.sample_text);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.INVISIBLE);

        // We need the old (pre-Android-11) file read permissions at once, otherwise
        // we cannot run on older Android versions. Later we may ask for full
        // file access on Android 11 and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            if (Environment.isExternalStorageManager())
            {
                Log.d(LOG_TAG, "onCreate(): full file access permission already granted");
                mFileReadPermissionGranted = true;
                mFileWritePermissionGranted = true;
            }
            else
            {
                if (UserSettings.getBool(UserSettings.PREF_DBG_REQUEST_MEDIA_FILE_ACCESS, false))
                {
                    Log.d(LOG_TAG, "onCreate(): check media read permission");
                    // In fact we can ask for READ permission to some (?) files, including audio, but
                    // we will never get WRITE permission.
                    requestForMediaReadPermission30();
                }
            }
        }
        else
        {
            requestForPermissionOld();
        }
        onFileAccessModesChanged();

        mFloatingButton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d(LOG_TAG, "onCreate() : scan directory");
                runAsyncCommand(actionType.actionAutoScan);
            /*
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
                    */
            }
        });

        mFloatingButton2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d(LOG_TAG, "onCreate() : scan directory");
                runAsyncCommand(actionType.actionRebuildAll);
            /*
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
                    */
            }
        });

        restoreSavedInstanceState(savedInstanceState);
        evalIntent();
    }


    /**************************************************************************
     *
     * called after onCreate() or after the application has been put back to
     * the foreground.
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    protected void onStart()
    {
        Log.d(LOG_TAG, "onStart()");

        // manage progress bar via timer
        mTimerTask = new MyTimerTask();

        //delay 1000ms, repeat in <timerFrequency>ms
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 1000, timerFrequency);

        super.onStart();
        refreshRequestReadPermission(); // test code
        Log.d(LOG_TAG, "onStart() - done");
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onStop()
    {
        Log.d(LOG_TAG, "onStop()");

        // stop timer
        mTimer.cancel();    // note that a cancelled timer cannot be re-scheduled. Why not?
        mTimer = null;
        mTimerTask = null;

        super.onStop();
        Log.d(LOG_TAG, "onStop() - done");
    }


    /************************************************************************************
     *
     * Activity method: save state
     *
     ***********************************************************************************/
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)
    {
        Log.d(LOG_TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);

        // save current text as string
        if (mStatusText != null)
        {
            Log.d(LOG_TAG, "onSaveInstanceState(): save text " + mStatusText);
            outState.putString(stateSavedStatusText, mStatusText);
        }
    }


    /************************************************************************************
     *
     * restore saved state
     *
     ***********************************************************************************/
    protected void restoreSavedInstanceState(Bundle savedInstanceState)
    {
        if (savedInstanceState != null)
        {
            String text = savedInstanceState.getString(stateSavedStatusText);
            if (text != null)
            {
                Log.d(LOG_TAG, "restoreSavedInstanceState(): restore saved text " + text);
                mStatusText = text;
            }
        }
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        final int[] debugItems =
        {
            R.id.action_query_audio_files,
            R.id.action_scan_directories,
            R.id.action_collect_files,
            R.id.action_query_albums,
            R.id.action_query_album_pictures,
            R.id.action_build_albums,
            R.id.action_truncate_audio_file_table,
            R.id.action_truncate_album_table,
            R.id.action_remove,
            R.id.action_delete_db,
            R.id.action_get_num,
            R.id.action_write_db_version,
            R.id.action_revoke_permissions
        };

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mbShowDebugMenuEntries = UserSettings.getBool(UserSettings.PREF_SHOW_DEBUG_MENU_ENTRIES, false);
        if (!mbShowDebugMenuEntries)
        {
            for (int item_no : debugItems)
            {
                MenuItem theMenuItem = menu.findItem(item_no);
                theMenuItem.setVisible(false);
            }
        }

        mMenuItemManageFiles = menu.findItem(R.id.action_manage_external_files);
        return true;
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * called everytime the menu opens
     * Enable and disable menu items according to state and context
     *
     *************************************************************************/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            mMenuItemManageFiles.setCheckable(true);
            mMenuItemManageFiles.setEnabled(true);
            //noinspection RedundantIfStatement
            if (Environment.isExternalStorageManager())
            {
                // full file access already granted
                mMenuItemManageFiles.setChecked(true);
            }
            else
            {
                mMenuItemManageFiles.setChecked(false);
            }
        }
        else
        {
            // The "manage files" permission is available for Android 10 and above
            mMenuItemManageFiles.setEnabled(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about)
        {
            DialogAbout();
            return true;
        }
        else
        if (id == R.id.action_changes)
        {
            DialogChanges();
        }
        else
        if (id == R.id.action_help)
        {
            DialogHelp();
        }
        else
        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(this, MyPreferenceActivity.class);
            // deprecated startActivityForResult(intent, RESULT_SETTINGS);
            mPreferencesActivityLauncher.launch(intent);
            return true;
        }
        else
        if (id == R.id.action_manage_external_files)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 10 and newer: MANAGE EXTERNAL FILES
                //noinspection ConstantConditions
                boolean isPlayStoreVersion = (BuildConfig.BUILD_TYPE.equals("release_play")) ||
                                             (BuildConfig.BUILD_TYPE.equals("debug_play"));
                //noinspection ConstantConditions
                if (isPlayStoreVersion && !Environment.isExternalStorageManager())
                {
                    dialogNotAvailableInPlayStoreVersion();
                }
                else
                {
                    requestForPermission30();
                }
            }

            return true;
        }
        else
        if (id == R.id.action_scan_directories)
        {
            runAsyncCommand(actionType.actionScanDirectories);
            return true;
        }
        else
        if (id == R.id.action_collect_files)
        {
            runAsyncCommand(actionType.actionCollectMedia);
            return true;
        }
        else
        if (id == R.id.action_query_audio_files)
        {
            runAsyncCommand(actionType.actionQueryAudioFiles);
            return true;
        }
        else
        if (id == R.id.action_query_albums)
        {
            runAsyncCommand(actionType.actionQueryAlbums);
            return true;
        }
        else
        if (id == R.id.action_query_album_pictures)
        {
            runAsyncCommand(actionType.actionQueryAlbumPictures);
            return true;
        }
        else
        if (id == R.id.action_build_albums)
        {
            runAsyncCommand(actionType.actionBuildAlbums);
            return true;
        }
        else
        if (id == R.id.action_truncate_audio_file_table)
        {
            runAsyncCommand(actionType.actionTruncateAudioFileTable);
            return true;
        }
        else
        if (id == R.id.action_truncate_album_table)
        {
            runAsyncCommand(actionType.actionTruncateAlbumTable);
            return true;
        }
        else
        if (id == R.id.action_remove)
        {
            runAsyncCommand(actionType.actionRemoveTables);
            return true;
        }
        else
        if (id == R.id.action_get_num)
        {
            runAsyncCommand(actionType.actionGetNumbers);
            return true;
        }
        else
        if (id == R.id.action_write_db_version)
        {
            runAsyncCommand(actionType.actionWriteDbVersion);
            return true;
        }
        else
        if (id == R.id.action_delete_db)
        {
            String dbPath = getDbPath();
            if (dbPath != null)
            {
                File f = new File(dbPath);
                if (f.delete())
                {
                    Log.d(LOG_TAG, "database file \"" + dbPath + "\" deleted.");
                }
            }
            return true;
        }
        else
        if (id == R.id.action_revoke_permissions)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            {
                revokeMediaReadPermission33();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /************************************************************************************
     *
     * Activity method: got new intent
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent intent)
    {
        Log.d(LOG_TAG, "onNewIntent(): intent action is " + intent.getAction());
        evalIntent();
        super.onNewIntent(intent);
    }


    /**************************************************************************
     *
     * called when tracks activity or settings activity has ended
     *
     *************************************************************************/
    /* deprecated
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case RESULT_SETTINGS:
                // check for changed settings, and apply them
                initDefaultPaths(true);     // re-read db and music paths from settings
                break;
        }
    }
    */


    /************************************************************************************
     *
     * evaluate intent
     *
     ***********************************************************************************/
    protected void evalIntent()
    {
        Intent intent = getIntent();

        // message from Music Tagger to update database due to changed audio files
        ArrayList<String> audioPathArrayList = intent.getStringArrayListExtra(stateParamPathTable);
        if (audioPathArrayList != null)
        {
            // immediately update database due to request from Music Tagger
            runAsyncCommand(actionType.actionUpdateFiles, audioPathArrayList, null, null);
        }

        // message from Music Player to create or update database
        String musicAndDbPath = intent.getStringExtra(stateParamMusicAndDbPath);
        if (musicAndDbPath != null)
        {
            String musicPath;
            String dbPath;
            if (musicAndDbPath.endsWith("/"))
            {
                // remove last char
                musicPath = musicAndDbPath.substring(0, musicAndDbPath.length() - 1);
                dbPath = musicAndDbPath + UserSettings.dbDirectory;
            }
            else
            {
                musicPath = musicAndDbPath;
                dbPath = musicAndDbPath + "/" + UserSettings.dbDirectory;
            }
            if (audioPathArrayList != null)
            {
                // immediately update database due to request from Music Tagger
                runAsyncCommand(actionType.actionAutoScan, null, musicPath, dbPath);
            }
        }
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = getString(R.string.app_name);
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        final String strVersion = "Version " + info.versionName + ((info.isDebug) ? " DEBUG" : "");

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(webView);
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHelp()
    {
        DialogHtml("help.html");
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogChanges()
    {
        DialogHtml("changes.html");
    }


    /**************************************************************************
     *
     * "Not Available in Play Store Version" dialogue
     *
     *************************************************************************/
    private void dialogNotAvailableInPlayStoreVersion()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.str_NeedManageFilesPermission));
        alertDialog.setMessage(getString(R.string.str_NotAvailableInPlayStoreVersion));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * Read file access related preferences and check for changes
     *
     *************************************************************************/
    private boolean readFileSystemRelatedPreferences()
    {
        boolean res = false;

        String prefMusicBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
        String prefDataBasePath = UserSettings.getString(UserSettings.PREF_DATA_BASE_PATH);
        boolean prefDbgRequestMediaFileAccess = UserSettings.getBool(UserSettings.PREF_DBG_REQUEST_MEDIA_FILE_ACCESS, false);

        if (Utilities.stringsDiffer(prefMusicBasePath, mPrefMusicBasePath))
        {
            mPrefMusicBasePath = prefMusicBasePath;
            res = true;
        }
        if (Utilities.stringsDiffer(prefDataBasePath, mPrefDataBasePath))
        {
            mPrefDataBasePath = prefDataBasePath;
            res = true;
        }
        if (prefDbgRequestMediaFileAccess != mPrefDbgRequestMediaFileAccess)
        {
            mPrefDbgRequestMediaFileAccess = prefDbgRequestMediaFileAccess;
            res = true;
        }

        return res;
    }


    /**************************************************************************
     *
     * get list of music paths and, if not set, set it to default value
     *
     *************************************************************************/
    private String getMediaPaths()
    {
        String currMusicBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
        if (currMusicBasePath == null)
        {
            // initial write of preference
            String defaultMusicBasePath = Utilities.getDefaultMusicBasePath(this);
            currMusicBasePath = UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, defaultMusicBasePath);
        }
        return currMusicBasePath;
    }


    /**************************************************************************
     *
     * debug helper
     *
     *************************************************************************/
    private String getDbPathPrivate()
    {
        String dbPath;

        // getFilesDir() -> /data/user/0/de.kromke.andreas.mediascanner/files
        //  This is the app's private directory. Without root access or within an
        //  emulation, it is not accessible at all, even not with MANAGE_ALL_FILES
        //  and even not with the device's file manager.
        //
        // getExternalFilesDir() -> /storage/emulated/0/Android/data/de.kromke.andreas.mediascanner/files
        //  This is the app's external, somewhat semi-public, directory. It is accessible
        //  with MANAGE_ALL_FILES, e.g. with the device's file manager.

        File basePath = Utilities.getPrivateDirectory(this);
        dbPath = basePath.getPath() + "/" + UserSettings.dbDirectory;
        return dbPath;
    }


    /**************************************************************************
     *
     * Get directory where the database file shall be stored.
     * If write access is granted, and path is not set, set it to default value.
     * Return null on failure.
     *
     *************************************************************************/
    private String getDbPath()
    {
        String currDatabasePath;
        boolean bIsPrivate = false;

        if (!mFileReadPermissionGranted)
        {
            Log.e(LOG_TAG, "getDbPath(): no permissions granted");
            return null;
        }

        //
        // debug helper: creating private db without album pictures (more or less useless)
        //  /data/user/0/de.kromke.andreas.mediascanner/files/ClassicalMusicDb/musicmetadata.db
        // Note that this file is not even accessible with the device's file manager, but
        //  in the emulator via the device manager.
        //

        if (!mFileWritePermissionGranted)
        {
            // debug stuff
            Log.w(LOG_TAG, "getDbPath(): no write permission, will continue with private db!");
            currDatabasePath = getDbPathPrivate();
            bIsPrivate = true;
        }
        else
        {
            // this is the expected way to go
            currDatabasePath = UserSettings.getString(UserSettings.PREF_DATA_BASE_PATH);
            if (currDatabasePath == null)
            {
                // initial write of preference
                String defaultSharedDbBasePath = Utilities.getDefaultSharedDbBasePath(this);
                currDatabasePath = UserSettings.getAndPutString(UserSettings.PREF_DATA_BASE_PATH, defaultSharedDbBasePath);
            }
        }

        // check if path exists and is writable

        if (currDatabasePath != null)
        {
            File dir = new File(currDatabasePath);
            if (!dir.isDirectory() && !dir.mkdirs())
            {
                Log.e(LOG_TAG, "getDbPath(): cannot create directories");
                currDatabasePath = null;
            }
            else
            if (bIsPrivate)
            {
                Toast.makeText(getApplicationContext(), R.string.str_private_db, Toast.LENGTH_LONG).show();
            }
        }

        return currDatabasePath;
    }


    /**************************************************************************
     *
     * update GUI accordingly
     *
     *************************************************************************/
    private void onFileAccessModesChanged()
    {
        if (mFileReadPermissionGranted && mFileWritePermissionGranted)
        {
            mStartStatusText = getString(R.string.str_start_button);
        }
        else
        if (mFileReadPermissionGranted)
        {
            mStartStatusText = getString(R.string.str_only_media_read_access_30);
        }
        else
        {
            mStartStatusText = getString((Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? R.string.str_no_access_sdk30 : R.string.str_no_access);
        }
        mStatusText = mStartStatusText;
        updateFloatingButton();
    }


    /**************************************************************************
     *
     * called when access request has been denied or granted
     * (old method for Android 4.4. .. 10 and for media files in Android 11 and newer)
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult
    (
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    )
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(LOG_TAG, "onRequestPermissionsResult()");

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            // Android 4.4 .. 10
            case MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                    mFileReadPermissionGranted = true;
                    mFileWritePermissionGranted = true;
                    onFileAccessModesChanged();
                } else
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission denied");
                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }

            // Android 11 and newer
            case MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): media read permission granted");
                    mFileReadPermissionGranted = true;
                    //mFileWritePermissionGranted = false;
                    onFileAccessModesChanged();
                } else
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): media read permission denied");
                }
            }
        }
    }


    /**************************************************************************
     *
     * variant for Android 4.4 .. 10
     *
     *************************************************************************/
    private void requestForPermissionOld()
    {
        String[] requests = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        int permissionCheck1 = ContextCompat.checkSelfPermission(this, requests[0]);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this, requests[1]);
        if ((permissionCheck1 == PackageManager.PERMISSION_GRANTED) && (permissionCheck2 == PackageManager.PERMISSION_GRANTED))
        {
            if (!mFileReadPermissionGranted || !mFileWritePermissionGranted)
            {
                // permissions have changed
                Log.d(LOG_TAG, "file r/w permissions immediately granted");
                mFileReadPermissionGranted = true;
                mFileWritePermissionGranted = true;
                onFileAccessModesChanged();
            }
        } else
        {
            Log.d(LOG_TAG, "request permission");
            ActivityCompat.requestPermissions(this, requests,
                    MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
        }
    }


    /**************************************************************************
     *
     * restart the app
     *
     *************************************************************************/
    private void restart()
    {
        Log.d(LOG_TAG, "restart()");
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
        if (intent != null)
        {
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(componentName);
            startActivity(mainIntent);
            System.exit(0);
        }
    }


    /**************************************************************************
     *
     * revoke permissions for Android 13 and newer (debug helper)
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    private void revokeMediaReadPermission33()
    {
        String[] requests = new String[] {
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES
            };

        revokeSelfPermissionsOnKill(Arrays.asList(requests));
        // -> https://issuetracker.google.com/issues/308540675
        //restart();      // <= this includes exit() and reproduces the bug
    }


    /**************************************************************************
     *
     * debug helper, test code
     *
     *************************************************************************/
    private void refreshRequestReadPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            if (Environment.isExternalStorageManager())
            {
                Log.d(LOG_TAG, "onCreate(): full file access permission already granted");
                if (!mFileReadPermissionGranted || !mFileWritePermissionGranted)
                {
                    mFileReadPermissionGranted = true;
                    mFileWritePermissionGranted = true;
                    onFileAccessModesChanged();
                }
            }
            else
            {
                if (UserSettings.getBool(UserSettings.PREF_DBG_REQUEST_MEDIA_FILE_ACCESS, false))
                {
                    Log.d(LOG_TAG, "onCreate(): check media read permission");
                    // In fact we can ask for READ permission to some (?) files, including audio, but
                    // we will never get WRITE permission.
                    requestForMediaReadPermission30();
                }
            }
        }
        else
        {
            requestForPermissionOld();
        }
        //onFileAccessModesChanged();
    }


    /**************************************************************************
     *
     * media read access for Android 11 and newer
     *
     * Note that this makes not much sense as we only can read audio files,
     * but not change them. Note that we also see images, but not if they
     * are detected as album art.
     *
     * Also note that Google's official Android documentation does not tell the
     * truth. Instead, in Android 11 WRITE_EXTERNAL_STORAGE is ignored,
     * and READ_EXTERNAL_STORAGE grants read access to media files.
     *
     *************************************************************************/
    private void requestForMediaReadPermission30()
    {
        String[] requests;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            // Android 13 needs different requests than 4..12
            requests = new String[] {
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES
            };
        }
        else
        {
            // this one is invalid in Android 13
            requests = new String[] {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE  // we might omit that
                    Manifest.permission.READ_EXTERNAL_STORAGE  // ask twice to just continue the code
            };
        }

        int permissionCheck1 = ContextCompat.checkSelfPermission(this, requests[0]);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this, requests[1]);
        if ((permissionCheck1 == PackageManager.PERMISSION_GRANTED) && (permissionCheck2 == PackageManager.PERMISSION_GRANTED))
        {
            if (!mFileReadPermissionGranted)
            {
                Log.d(LOG_TAG, "media read permissions immediately granted");
                mFileReadPermissionGranted = true;
                //mFileWritePermissionGranted = false;
                onFileAccessModesChanged();
            }
        } else
        {
            Log.d(LOG_TAG, "request media read permission");
            // note that callback will be onRequestPermissionsResult()
            ActivityCompat.requestPermissions(this, requests,
                    MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES);
        }
    }


    /**************************************************************************
     *
     * variant for Android 11 (API 30)
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.R)
    protected void requestForPermission30()
    {
        /*
        if (Environment.isExternalStorageManager())
        {
            Log.d(LOG_TAG, "permission immediately granted");
            setupPathList();
            mReadWritePermissionGranted = true;
        }
        else
        */
        {
            Intent intent = new Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", this.getPackageName(), null);
            intent.setData(uri);
            mStorageAccessPermissionActivityLauncher.launch(intent);
        }
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerPreferencesCallback()
    {
        mPreferencesActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>()
                {
                    @Override
                    public void onActivityResult(ActivityResult result)
                    {
                        boolean bShowDebugMenuEntries = UserSettings.getBool(UserSettings.PREF_SHOW_DEBUG_MENU_ENTRIES, false);
                        if (bShowDebugMenuEntries != mbShowDebugMenuEntries)
                        {
                            // setting change needs restart
                            restart();
                        }

                        // check for changed settings, and apply them
                        if (readFileSystemRelatedPreferences())
                        {
                            Log.d(LOG_TAG, "onActivityResult(): file system related prefs changed");
                        }
                    }
                });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     * This is for Android 11 and newer and handles MANAGE_EXTERNAL_STORAGE.
     *
     * Note that onActivityResult() will not be called when the permission
     * is revoked, because at that very moment the process is killed. It is
     * restarted when the user then exists the system's permission dialogue.
     *
     * On the other hand the callback IS called when the user exits
     * this dialogue without changing the permissions.
     *
     *************************************************************************/
    private void registerStorageAccessPermissionCallback()
    {
        mStorageAccessPermissionActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>()
                {
                    @Override
                    @RequiresApi(api = Build.VERSION_CODES.R)
                    public void onActivityResult(ActivityResult result)
                    {
                        boolean bReadPermissionGrantedNew;
                        boolean bWritePermissionGrantedNew;

                        // Note that the resultCode is not helpful here, fwr
                        if (Environment.isExternalStorageManager())
                        {
                            Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): r/w permission granted");
                            bReadPermissionGrantedNew = true;
                            bWritePermissionGrantedNew = true;
                        }
                        else
                        {
                            Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): w permission denied");
                            // Note that file read access may still be possible via READ_EXTERNAL_STORAGE or READ_MEDIA_AUDIO
                            bReadPermissionGrantedNew = mFileReadPermissionGranted;
                            bWritePermissionGrantedNew = false;
                        }

                        if ((bReadPermissionGrantedNew != mFileReadPermissionGranted) ||
                                (bWritePermissionGrantedNew != mFileWritePermissionGranted))
                        {
                            // just granted or denied
                            mFileReadPermissionGranted = bReadPermissionGrantedNew;
                            mFileWritePermissionGranted = bWritePermissionGrantedNew;
                            onFileAccessModesChanged();
                            if (!mFileWritePermissionGranted)
                            {
                                Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }


    /**************************************************************************
     *
     * update action bar
     *
     *************************************************************************/
    void updateFloatingButton()
    {
        if (mFloatingButton1 != null)
        {
            int id = (mbAsyncTaskBusy) ? R.color.action_button_scan_busy_colour : R.color.action_button_scan_colour;
            ColorStateList tint = AppCompatResources.getColorStateList(this, id);
            mFloatingButton1.setBackgroundTintList(tint);
            if (mStatusText.isEmpty())
            {
                //Log.d(LOG_TAG, "updateFloatingButton() : status is empty");
                mText.setText(mStartStatusText);
            } else
            {
                //Log.d(LOG_TAG, "updateFloatingButton() : status is \"" + mStatusText + "\"");
                mText.setText(mStatusText);
            }
        }

        if (mFloatingButton2 != null)
        {
            int id = (mbAsyncTaskBusy) ? R.color.action_button_manualscan_busy_colour : R.color.action_button_manualscan_colour;
            ColorStateList tint = AppCompatResources.getColorStateList(this, id);
            mFloatingButton2.setBackgroundTintList(tint);
        }
    }


    /**************************************************************************
     *
     * async task has finished with success (0) or error
     *
     *************************************************************************/
    private void onCollectFinished(Integer result)
    {
        Log.d(LOG_TAG, "onCollectFinished(" + result + ")");
        String sresult = (result == 0) ? getString(R.string.str_complete) : getString(R.string.str_failed);
        Toast.makeText(getApplicationContext(),
                getString(R.string.str_scanning_uc) + " " + sresult, Toast.LENGTH_SHORT)
                .show();
        if (mNumAudioFiles > 0)
        {
            mProgressBar.setProgress(result == 0 ? 100 : 0);
        }
    }


    /**************************************************************************
     *
     * start async task for file operations
     *
     *************************************************************************/
    private void runAsyncCommand(actionType theAction)
    {
        runAsyncCommand(theAction, null, null, null);
    }


    /**************************************************************************
     *
     * start async task for file operations
     *
     *************************************************************************/
    private void runAsyncCommand
    (
        actionType theAction,
        ArrayList<String> audioPathArrayList,
        String musicPath,
        String dbPath
    )
    {
        if (mbAsyncTaskBusy)
        {
            Log.e(LOG_TAG, "runAsyncCommand() : task is busy");
            return;
        }

        String mediaPaths;
        if (theAction == actionType.actionUpdateFiles)
        {
            // no paths, instead just a table of audio files in mAudioPathArrayList
            mediaPaths = null;
        }
        else
        {
            if (musicPath == null)
            {
                mediaPaths = getMediaPaths();
            }
            else
            {
                mediaPaths = musicPath;
            }
            if (mediaPaths == null)
            {
                Log.e(LOG_TAG, "runAsyncCommand() : no media paths");
                return;
            }
        }

        if (dbPath == null)
        {
            dbPath = getDbPath();
        }
        if (dbPath == null)
        {
            Log.e(LOG_TAG, "runAsyncCommand() : no db path");
            return;
        }

        // remember that async task is running
        mbAsyncTaskBusy = true;
        updateFloatingButton();
        if (mProgressBar != null)
        {
            mProgressBar.setProgress(0);
            mProgressBar.setVisibility(View.VISIBLE);
        }
/*
        Toast.makeText(getApplicationContext(),
                "Start reading tags ...", Toast.LENGTH_SHORT)
                .show();
*/
        FileAccessTaskDoParams params = new FileAccessTaskDoParams();
        params.actionCode = theAction;
        params.bExtractFolderIcons = UserSettings.getBool(UserSettings.PREF_EXTRACT_FOLDER_IMAGES, true);
        params.dbFile = dbPath + "/" + dbfilename;
        params.mediaPaths = mediaPaths;     // maybe null
        params.audioPathArrayList = audioPathArrayList;     // maybe null
        params.albumPicFname = UserSettings.getAlbumPicFname();
        params.flags = UserSettings.getBool(UserSettings.PREF_IGNORE_NOMEDIA, false) ? 0 : 1;

        new FileAccessTask().execute(params);
    }


    /**********************************************************************************************
     *
     * timer callback is run once per second
     *
     *********************************************************************************************/
    static int toggle = 0;
    final static int maxtoggle = 4;
    public void TimerCallback()
    {
        if (mbAsyncTaskBusy)
        {
            String text = mStatusText;
            text += mCallbackArgAudioFilesProcessed + " " + getString(R.string.str_audio_files_processed);

            if (mCallbackArgDirectories != 0)
            {
                text += " " + getString(R.string.str_in) + " " + mCallbackArgDirectories + " " + getString(R.string.str_directories_dativ);
            }
            text += ".";
            if (mCallbackArgAlbums != 0)
            {
                text += "\n" + mCallbackArgAlbums + " " + getString(R.string.str_albums_found) + ".";
            }
            if (mCallbackArgFolderPicturesFound != 0)
            {
                text += "\n" + mCallbackArgFolderPicturesFound + " " + getString(R.string.str_pic_files_found) + ".";
            }
            if (mCallbackArgPicturesExtracted != 0)
            {
                text += "\n" + mCallbackArgPicturesExtracted + " " + getString(R.string.str_album_pictures_extracted) + ".";
            }
            if (mCallbackArgPicturesExtractionFailed != 0)
            {
                text += "\n" + mCallbackArgPicturesExtractionFailed + " " + getString(R.string.str_album_pictures_not_extracted) + ".";
            }
            if (mCallbackArgFolderPicturesScaled != 0)
            {
                text += "\n" + mCallbackArgFolderPicturesScaled + " " + getString(R.string.str_existing_album_pictures_downscaled) + ".";
            }
            if (mCallbackArgFolderPicturesRenamed != 0)
            {
                text += "\n" + mCallbackArgFolderPicturesRenamed + " " + getString(R.string.str_pic_files_renamed) + ".";
            }
            final String tanim = "///_///_///_///_///_///_///_///_///_///";
            text += "\n" + tanim.substring(maxtoggle - toggle, tanim.length() - toggle);

            toggle++;
            toggle %= maxtoggle;
            //Log.d(LOG_TAG, "mAudioFiles = " + mAudioFiles + ", mCallbackArgAudioFilesProcessed = " + mCallbackArgAudioFilesProcessed);
            if (mNumAudioFiles > 0)
            {
                mProgressBar.setProgress((mCallbackArgAudioFilesProcessed * 100) / mNumAudioFiles);
            }
            mText.setText(text);
        }
    }


    @SuppressWarnings("Anonymous2MethodRef")
    class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    TimerCallback();
                }
            });
        }

    }


    /**************************************************************************
     *
     * called from JNI
     *
     * msg == 0: progress
     *        1: final result
     *        >1: success
     *        <0: failure
     *
     *************************************************************************/
    public void callbackFromScanner(int msg, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8)
    {
        mFinalStatusText = "";
        switch(msg)
        {
            // progress
            case ScannerManager.eResultProgress:
                mCallbackArgAudioFilesProcessed = arg1;
                mCallbackArgDirectories = arg2;
                mCallbackArgAlbums = arg3;
                mCallbackArgPicturesExtracted = arg4;
                mCallbackArgPicturesExtractionFailed = arg5;
                mCallbackArgFolderPicturesScaled = arg6;
                mCallbackArgFolderPicturesRenamed = arg7;
                mCallbackArgFolderPicturesFound = arg8;
                break;

            case ScannerManager.eResultMalformedPath:
                mFinalStatusText = getString(R.string.str_err_malformed_path);
                break;

            case ScannerManager.eResultSqlError:
                mFinalStatusText = getString(R.string.str_err_sql_problem);
                break;

            case ScannerManager.eResultCannotOpenDb:
                mFinalStatusText = getString(R.string.str_err_cannot_open_db);
                break;

            case ScannerManager.eResultInvalidCmd:
                mFinalStatusText = getString(R.string.str_err_invalid_cmd);
                break;

            case ScannerManager.eResultFileScan:
                mFinalStatusText = arg1 + " " + getString(R.string.str_audio_files_found) + " " + getString(R.string.str_in) + " " + arg2 + " " + getString(R.string.str_directories_dativ) + ".";
                break;

            case ScannerManager.eResultFileProcess:
                mCallbackArgAudioFilesProcessedFinal = arg1;
                mFinalStatusText = arg1 + " " + getString(R.string.str_audio_files_processed) + ".";
                break;

            case ScannerManager.eResultDbCreatedOrOpened:
                mFinalStatusText = getString(R.string.str_db_created_opened);
                break;

            case ScannerManager.eResultDone:
                break;

            case ScannerManager.eResultAlbumScan:
                mFinalStatusText = arg3 + " " + getString(R.string.str_albums_found) + " " + getString(R.string.str_from) + " " + arg1 + " " + getString(R.string.str_audio_files) + ".\n";
                if (arg4 > 0)
                {
                    mFinalStatusText += "\n" + arg4 + " " + getString(R.string.str_album_pictures_extracted) + ".";
                }
                if (arg5 > 0)
                {
                    mFinalStatusText += "\n" + arg5 + " " + getString(R.string.str_album_pictures_not_extracted) + ".";
                }
                if (arg6 > 0)
                {
                    mFinalStatusText += "\n" + arg6 + " " + getString(R.string.str_existing_album_pictures_downscaled) + ".";
                }
                if (arg7 > 0)
                {
                    mFinalStatusText += "\n" + arg7 + " " + getString(R.string.str_pic_files_renamed) + ".";
                }
                if (arg8 > 0)
                {
                    mFinalStatusText += "\n" + arg8 + " " + getString(R.string.str_pic_files_found) + ".";
                }
                break;

            case ScannerManager.eResultTablesRemoved:
                mFinalStatusText = getString(R.string.str_tables_removed);
                break;

            case ScannerManager.eResultAlbumPicturesUpdated:
                mFinalStatusText = getString(R.string.str_album_pictures_added) + ": " + arg1 + ", " + getString(R.string.str_removed) + ": " + arg2 + ", " + getString(R.string.str_scaled) + ": " + arg3 + "\n";
                break;
        }
    }


    public static class FileAccessTaskDoParams
    {
        actionType actionCode;
        boolean bExtractFolderIcons;
        String dbFile;
        String mediaPaths;
        ArrayList<String> audioPathArrayList;   // for updating single audio files in database
        String albumPicFname;
        int flags;
    }

    /**************************************************************************
     *
     * async task for file operations
     *
     *************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class FileAccessTask extends AsyncTaskExecutor<FileAccessTaskDoParams /* do */, Integer /* pre */, Integer /* post */>
    {
        actionType actionCode;
        int iExtractFolderIcons;
        ScannerManager scanner;

        @Override
        protected void onPreExecute()
        {
            Log.d(LOG_TAG, "onPreExecute()");
        }


        @Override
        protected Integer doInBackground(FileAccessTaskDoParams... params)
        {
            actionCode = params[0].actionCode;
            iExtractFolderIcons = params[0].bExtractFolderIcons ? 1 : 0;
            String dbFile = params[0].dbFile;
            String albumpicfname = params[0].albumPicFname;
            int flags = params[0].flags;

            Log.d(LOG_TAG, "doInBackground(" + actionCode + ")");

            scanner = new ScannerManager()
            {
                public void jni_callback_progress(int msg, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7, int arg8)
                {
                    //Log.d(LOG_TAG, "FileAccessTask::doInBackground() : callback(msg = " + msg + ", arg1 = " + arg1 + ")");
                    callbackFromScanner(msg, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
                    /*
                    if (scanner.mNoOfImageFilesRenamed > 0)
                    {
                        mFinalStatusText += "" + scanner.mNoOfImageFilesRenamed + " " + getString(R.string.str_pic_files_renamed) + ".\n";
                    }
                    */
                }
            };

            int res = 0;
            switch (actionCode)
            {
                //
                // process update messages from tagger
                //

                case actionUpdateFiles:
                    // concat paths with LF
                    ArrayList<String> audioPathArrayList = params[0].audioPathArrayList;
                    mNumAudioFiles = audioPathArrayList.size();
                    int i;
                    StringBuilder sb = new StringBuilder(1024);
                    for (i = 0; i < mNumAudioFiles; i++)
                    {
                        sb.append(audioPathArrayList.get(i));
                        if (i < mNumAudioFiles - 1)
                        {
                            sb.append('\n');
                        }
                    }

                    mStatusText = "updating:\n" + sb + "\n";

                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanUpdateSpecifiedAudioFiles,
                                                    dbFile, sb.toString(), albumpicfname, flags);
                    mStatusText += mFinalStatusText + "\n";
                    break;

                //
                // Rebuild everything from scratch, i.e. create database file, if necessary, and remove and recreate tables
                //
                // This will also update the db version automatically, if necessary
                //

                case actionRebuildAll:
                    res = doRebuildAll(scanner, params[0].mediaPaths, dbFile);
                    break;

                //
                // auto scan, i.e. process changes, deletions and new files
                //

                case actionAutoScan:
                    res = doAutoScan(scanner, params[0].mediaPaths, dbFile);
                    if (res != 0)
                    {
                        // error: do full scan instead
                        Log.d(LOG_TAG, "doInBackground(actionAutoScan) : Auto scan failed, doing full scan instead");
                        mStatusText += "\n\nProblem encountered.\n";
                        mStatusText += "Doing full scan instead...\n";
                        res = doRebuildAll(scanner, params[0].mediaPaths, dbFile);
                    }
                    break;

                case actionScanDirectories:
                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles,
                            null, params[0].mediaPaths, albumpicfname, flags);
                    mStatusText = mFinalStatusText;
                    break;

                case actionCollectMedia:
                    res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectAllAudioFiles,
                            dbFile, params[0].mediaPaths, albumpicfname, flags);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAudioFiles:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniQueryAudioFiles, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAlbums:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniQueryAlbums, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionQueryAlbumPictures:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniUpdateAlbumPictures, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionBuildAlbums:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniCollectAlbums, iExtractFolderIcons, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionTruncateAudioFileTable:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniTruncateAudioFileTable, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionTruncateAlbumTable:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniTruncateAlbumTable, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionRemoveTables:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniRemoveTables, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionWriteDbVersion:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniWriteDbVersion, 0, albumpicfname);
                    mStatusText = mFinalStatusText;
                    break;

                case actionGetNumbers:
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniGetDbVersion, 0, albumpicfname);
                    mStatusText = "Database version: " + res + "\n";
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniGetNumAudioFiles, 0, albumpicfname);
                    mStatusText += "Number of audio files in table: " + res + "\n";
                    res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniGetNumAlbums, 0, albumpicfname);
                    mStatusText += "Number of albums in table: " + res + "\n";
                    break;
            }

            scanner = null;     // no longer needed
            return res;
        }


        @Override
        protected void onPostExecute(Integer result)
        {
            Log.d(LOG_TAG, "onPostExecute(" + result + ")");
            if ((actionCode == actionType.actionRebuildAll) ||
                (actionCode == actionType.actionBuildAlbums) ||
                (actionCode == actionType.actionAutoScan))
            {
                MainActivity.this.onCollectFinished(result);
            }
            mbAsyncTaskBusy = false;
            if (mFloatingButton1 == null)
            {
                // in batch mode
                System.exit(0);
            }
            updateFloatingButton();
        }


        /**************************************************************************
         *
         * async task: helper
         *
         *************************************************************************/
        private int doRebuildAll(ScannerManager scanner, final String mediaPaths, final String dbFile)
        {
            int res;

            mStatusText = "";
            mNumAudioFiles = 0;
            String albumpicfname = UserSettings.getAlbumPicFname();
            int flags = UserSettings.getBool(UserSettings.PREF_IGNORE_NOMEDIA, false) ? 0 : 1;

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles, null, mediaPaths, albumpicfname, flags);
            mStatusText = mFinalStatusText + "\n";
            if (res < 0)
            {
                return -1;     // failure
            }

            mNumAudioFiles = res;
            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniCreateDb, 0, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -2;     // failure
            }

            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniRemoveTables, 0, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -3;     // failure
            }

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectAllAudioFiles, dbFile, mediaPaths, albumpicfname, flags);
            if (res < 0)
            {
                return -4;     // failure
            }

            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniCollectAlbums, iExtractFolderIcons, albumpicfname);
            mStatusText += mFinalStatusText + "\n";
            if (res < 0)
            {
                return -5;     // failure
            }

            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniWriteDbVersion, 0, albumpicfname);

            return res;
        }


        /**************************************************************************
         *
         * async task: helper
         *
         *************************************************************************/
        private int doAutoScan(ScannerManager scanner, final String mediaPaths, final String dbFile)
        {
            int res;
            final String albumpicfname = UserSettings.getAlbumPicFname();
            int flags = UserSettings.getBool(UserSettings.PREF_IGNORE_NOMEDIA, false) ? 0 : 1;

            //
            // intro: check DB version
            //

            mStatusText = "";
            mNumAudioFiles = 0;
            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniCheckDbVersion, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Check Db version failed");
                return -1;     // failure
            }
            if (res > 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Db version mismatch");
                mStatusText = getString(R.string.str_db_version_mismatch) + "\n";
                return - 2;
            }

            //
            // count number of audio files on device
            //

            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCountAllAudioFiles, null, mediaPaths, albumpicfname, flags);
            mStatusText = mFinalStatusText + "\n";
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : file scan failed");
                return -3;     // failure
            }
            mNumAudioFiles = res;

            //
            // count number of files in table
            //

            mCallbackArgAudioFilesProcessed = 0;
            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniGetNumAudioFiles, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : Get audio file table length failed");
                return -4;     // failure
            }
            int nFilesInTable = res;

            //
            // if number of new or deleted files is too high, do a rebuild instead
            //

            if (mNumAudioFiles > nFilesInTable)
            {
                if (mNumAudioFiles - nFilesInTable > nFilesInTable / 4)
                {
                    mStatusText = getString(R.string.str_too_may_new_files) + "\n";
                    return 1;       // too many new files
                }
            }
            else
            {
                if (nFilesInTable - mNumAudioFiles > nFilesInTable / 3)
                {
                    mStatusText = getString(R.string.str_too_many_deleted_files) + "\n";
                    return 2;       // too many deleted files
                }
            }

            //
            // handle changed and deleted audio files from table
            //

            mCallbackArgAudioFilesProcessed = 0;
            mStatusText = getString(R.string.str_looking_for_chg_or_del_files) + "\n";
            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniAutoScan, 0, albumpicfname);
            if (res < 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : looking for deleted or changed files failed");
                return -5;     // failure
            }
            if (res > 0)
            {
                Log.d(LOG_TAG, "doAutoScan() : too many changes");
                mStatusText = "Too many changed files. Do a rebuild" + "\n";
                return 3;       // too many changed files
            }

            int changed = mCallbackArgAudioFilesProcessedFinal;
            mCallbackArgAudioFilesProcessed = 0;
            mStatusText = getString(R.string.str_changed_or_removed_files) + " " + changed + "\n";

            //
            // handle new audio files (not yet in table)
            //

            mCallbackArgAudioFilesProcessed = 0;
            String s = mStatusText; // save
            mStatusText += getString(R.string.str_looking_for_new_files) + "\n";
            res = scanner.scanDirectoryJNI(ScannerManager.cmdJniScanCollectNewAudioFiles, dbFile, mediaPaths, albumpicfname, flags);
            if (res >= 0)
            {
                mStatusText = s + getString(R.string.str_new_files) + " " + res + "\n";
                //res = 0;        // no error
            }

            //
            // handle removed or updated album pictures
            //

            s = mStatusText; // save
            mStatusText += getString(R.string.str_checking_album_pictures) + "\n";
            res = scanner.doTableJNI(dbFile, ScannerManager.cmdJniUpdateAlbumPictures, 0, albumpicfname);
            if (res >= 0)
            {
                mStatusText = s + mFinalStatusText + "\n";
                res = 0;        // no error
            }

            return res;
        }
    }
}
