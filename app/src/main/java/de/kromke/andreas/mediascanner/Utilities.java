/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.content.Context;
import android.os.Environment;

import android.util.Log;

import java.io.File;

import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStorageState;


/** @noinspection JavadocLinkAsPlainText, JavadocBlankLines, RedundantSuppression , DanglingJavadoc , CommentedOutCode */
@SuppressWarnings("Convert2Lambda")
public class Utilities
{
    private static final String LOG_TAG = "CMS : Utilities";

    /**************************************************************************
     *
     * safe way to check if string differ
     *
     *************************************************************************/
    static boolean stringsDiffer(final String s1, final String s2)
    {
        if (s1 == null)
        {
            return s2 != null;
        }
        // now s1 is not null
        return !s1.equals(s2);
    }


    /************************************************************************************
     *
     * Method to get paths of all SD cards. Note that there is no really clean way
     * to do this.
     *
     * On LG G8s (Android 11) this method also finds USB memory attached via OTG.
     * On Samsung (Android 11) and Xiaomi (Android 13) and some older devices
     * (Android 8.1 and 6) OTG memory cannot be found and is only available via
     * SAF or maybe secret methods.
     *
     ***********************************************************************************/
    private static File[] getSdCardMusicPaths(Context context)
    {
        final String prefix = "/storage/";

        // get private (!) music paths
        File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
        // Entry #0 is the internal memory, the others are SD cards or USB memory.
        for (int i = 0; i < files.length; i++)
        {
            File f = files[i];
            files[i] = null;

            if (i == 0)
            {
                continue;       // remove first entry, this is the internal memory
            }

            if (f == null)
            {
                Log.e(LOG_TAG, "getSdCardMusicPaths() : got null path from OS?!?");
                continue;       // bug in this Android device?
            }

            String path = f.getPath();
            Log.d(LOG_TAG, "getSdCardMusicPaths() : found external path " + path);
            if (path.startsWith(prefix))
            {
                // find first '/' after the prefix path
                int index = path.indexOf('/', prefix.length());
                if (index >= 0)
                {
                    path = path.substring(0, index + 1) + "/Music";
                    files[i] = new File(path);
                }
            }
        }

        return files;
    }


    /* ***********************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    /*
    public static boolean isSymlink(File file) throws IOException
    {
        File canon;
        if (file.getParent() == null) {
            canon = file;
        } else {
            File canonDir = file.getParentFile().getCanonicalFile();
            canon = new File(canonDir, file.getName());
        }
        return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
    }
    */


    /* ***********************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    /*
    private void getRootPaths()
    {
        File mnt = new File("/storage");
        if (!mnt.exists())
        {
            mnt = new File("/mnt");
        }

        File[] roots = mnt.listFiles(new FileFilter()
        {
            @Override
            public boolean accept(File pathname)
            {
                Log.d(LOG_TAG, "check path " + pathname.getPath());
                boolean bIsSymlink = false;
                boolean bIsInvalid = false;
                boolean bCanWrite = pathname.canWrite();
                try
                {
                    bIsSymlink = isSymlink(pathname);
                }
                catch(Exception e)
                {
                    bIsInvalid = true;
                }

                if (bIsInvalid)
                    Log.d(LOG_TAG, "this is invalid");

                if (bIsSymlink)
                    Log.d(LOG_TAG, "this is a symlink");

                if (!bCanWrite)
                    Log.d(LOG_TAG, "cannot write");

                return pathname.isDirectory() && pathname.exists()
                        && bCanWrite && !pathname.isHidden()
                        && !bIsSymlink && !bIsInvalid;
            }
        });

        printPaths(roots, "roots");
    }
    */


    /************************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    @SuppressWarnings("SameParameterValue")
    private static void printPaths(File[] files, final String name)
    {
        int i = 0;
        for (File f:files)
        {
            Log.d(LOG_TAG, "" + name + "[" + i + "] = " + ((f == null) ? "null" : f.getPath()));
            i++;
        }
    }


    /************************************************************************************
     *
     * get music base path from environment or from settings
     *
     * getFilesDir() and getCacheDir() return the internal private paths,
     *  i.e. /data/user/0/de.kromke.andreas.mediascanner/files and .../cache
     * They are not accessible, even by file managers.
     *
     * getExternalFilesDir(Environment.DIRECTORY_MUSIC) also returns some
     *  internal private path, i.e. /storage/emulated/0/Android/data/de.kromke.andreas.mediascanner/files/Music
     * They are accessible only by file managers.
     *
     * getExternalFilesDirs() does the same, but also returns a second path, i.e.
     *  /storage/42B5-1A05/Android/data/de.kromke.andreas.mediascanner/files/Music
     *
     * getExternalStorageDirectory() returns the root of the internal public
     *  memory, i.e.: /storage/emulated/0
     *
     * System.getenv("SECONDARY_STORAGE") seems only to work on Samsung devices.
     *
     ***********************************************************************************/
    public static String getDefaultMusicBasePath(Context context)
    {
        File[] files;

        // methods from Activity

        /*
        Log.d(LOG_TAG, "getFilesDir(): " + getFilesDir().getPath());
        Log.d(LOG_TAG, "getCacheDir(): " + getCacheDir().getPath());
        Log.d(LOG_TAG, "getExternalFilesDir(Environment.DIRECTORY_MUSIC): " + getExternalFilesDir(Environment.DIRECTORY_MUSIC).getPath());
        //Log.d(LOG_TAG, "getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS): " + getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getPath());
        Log.d(LOG_TAG, "getExternalStorageDirectory(): " + getExternalStorageDirectory().getPath());

        // methods from Environment

        Log.d(LOG_TAG, "System.getenv(\"SECONDARY_STORAGE\") : " + System.getenv("SECONDARY_STORAGE"));
        Log.d(LOG_TAG, "Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC): " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath());
        files = getExternalFilesDirs(null);
        printPaths(files, "getExternalFilesDirs(null)");
        files = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
        printPaths(files, "getExternalFilesDirs(Environment.DIRECTORY_MUSIC)");

        // brute force
        getRootPaths();
        */

        //
        // get default value from environment
        //

        File basePath;
        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        } else
        {
            basePath = new File("/");
        }

        files = getSdCardMusicPaths(context);
        files[0] = basePath;
        printPaths(files, "music paths");

        // concat paths
        String defaultPath = "";
        boolean isFirst = true;
        for (File f:files)
        {
            if (f != null)
            {
                if (isFirst)
                {
                    isFirst = false;
                } else
                {
                    //noinspection StringConcatenationInLoop
                    defaultPath += "\n";
                }
                //noinspection StringConcatenationInLoop
                defaultPath += f.getPath();
            }
        }

        Log.d(LOG_TAG, "getDefaultMusicBasePath() : " + defaultPath);
        return defaultPath;
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    public static File getPrivateDirectory(Context context)
    {
        File basePath = context.getExternalFilesDir(null);   // accessible by file managers
        if (basePath == null)
        {
            basePath = context.getFilesDir();   // not accessible without root
        }
        return basePath;
    }


    /************************************************************************************
     *
     * get db base path from environment or from settings
     *
     ***********************************************************************************/
    public static String getDefaultSharedDbBasePath(Context context)
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = getExternalStorageDirectory();
        } else
        {
            basePath = getPrivateDirectory(context);
        }

        String defaultPath = basePath.getPath() + "/" + UserSettings.dbDirectory;
        Log.d(LOG_TAG, "getDefaultSharedDbBasePath() : " + defaultPath);
        return defaultPath;
    }

}
