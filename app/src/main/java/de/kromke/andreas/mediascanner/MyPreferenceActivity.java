/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

/*
 * minimalistic preferences
 */

import static android.os.Environment.getExternalStorageState;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.io.File;

/** @noinspection Convert2Lambda, JavadocBlankLines, RedundantSuppression */
public class MyPreferenceActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "CMS : MyPrefActivity";

    /**************************************************************************
     *
     * helper to get path from URI
     *
     * /tree/primary:Music/bla
     * /tree/12F0-3703:Music/bla
     *
     *************************************************************************/
    static String pathFromTreeUri(Uri uri)
    {
        String uriPath = uri.getPath();
        if (uriPath != null)
        {
            if (uriPath.startsWith("/tree/primary:"))
            {
                return "/storage/emulated/0/" + uriPath.substring(14);
            }

            if ((uriPath.length() >= 17) &&
                (uriPath.substring(0, 16).matches("^/tree/....-....:$")))
            {
                return "/storage/" + uriPath.substring(6, 15) + "/" + uriPath.substring(16);
            }
        }

        return null;
    }



    /** @noinspection Convert2Lambda, JavadocBlankLines */
    // ATTENTION: This class must be static, otherwise CRASH!
    public static class MySettingsFragment extends PreferenceFragmentCompat
    {
        ActivityResultLauncher<Intent> mOpenDocumentTreeActivityLauncher;

        /* *************************************************************************
         *
         * helper for deprecated startActivityForResult()
         *
         * Note that ActivityResultContracts.OpenDocumentTree() is nonsense,
         * because we have to pass some flags in our intent:
         *
         * ActivityResultContracts.OpenDocumentTree(): An ActivityResultContract
         * to prompt the user to select a directory, returning the user selection
         * as a Uri. Apps can fully manage documents within the returned directory.
         *
         * The input is an optional Uri of the initial starting location.
         *
         ************************************************************************/
        private void registerOpenDocumentTreeCallback()
        {
            mOpenDocumentTreeActivityLauncher = registerForActivityResult(
                    /* new ActivityResultContracts.OpenDocumentTree(), NONSENSE */
                    new ActivityResultContracts.StartActivityForResult(),
                    /* new ActivityResultCallback<Uri>() NONSENSE */
                    new ActivityResultCallback<ActivityResult>()
                    {
                        @Override
                        /* public void onActivityResult(Uri treeUri) NONSENSE */
                        public void onActivityResult(ActivityResult result)
                        {
                            int resultCode = result.getResultCode();
                            Intent data = result.getData();

                            if ((resultCode == RESULT_OK) && (data != null))
                            {
                                Uri treeUri = data.getData();
                                if (treeUri != null)
                                {
                                    Log.d(LOG_TAG, " URI path = " + treeUri.getPath());
                                    // /tree/primary:Music/bla
                                    // /tree/12F0-3703:Music/bla
                                    String newMusicBasePath = pathFromTreeUri(treeUri);
                                    Log.d(LOG_TAG, " real Path = " + newMusicBasePath);
                                    if (newMusicBasePath != null)
                                    {
                                        File f = new File(newMusicBasePath);
                                        if (f.exists())
                                        {
                                            String newSharedDbBasePath = newMusicBasePath + "/" + UserSettings.dbDirectory;
                                            if (changePaths(getActivity(), newMusicBasePath, newSharedDbBasePath))
                                            {
                                                Log.d(LOG_TAG, " new DB path = " + newSharedDbBasePath);
                                                Toast.makeText(getActivity(), R.string.str_paths_updated, Toast.LENGTH_LONG).show();
                                                updateGui();
                                                return;
                                            }
                                        }
                                    }
                                    Toast.makeText(getActivity(), R.string.str_paths_unchanged, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
        }


        /**************************************************************************
         *
         * helper to start SAF file selector for a document tree
         *
         *************************************************************************/
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        protected void startSafPickerForDocumentTree()
        {
            final String intent_code = Intent.ACTION_OPEN_DOCUMENT_TREE;
            Intent intent = new Intent(intent_code);

            // specify initial directory tree position
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String uriString = mDir.getCurrentUriAsString();
            if (uriString != null)
            {
                Uri uri = Uri.parse(uriString);
                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
            }
        }
        */

            // Ask for read and write access to files and sub-directories in the user-selected directory.
            intent.addFlags(
                    Intent.FLAG_GRANT_READ_URI_PERMISSION +
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION +
                            Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                            Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

            mOpenDocumentTreeActivityLauncher.launch(intent);
        }


        /************************************************************************************
         *
         * Helper to change the paths
         *
         ***********************************************************************************/
        private boolean changePaths(Context context, String newMusicBasePath, String newSharedDbBasePath)
        {
            boolean ret = false;
            UserSettings.setContextIfNecessary(context);

            // get current values
            String currMusicBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
            String currDatabasePath = UserSettings.getString(UserSettings.PREF_DATA_BASE_PATH);

            // check if there are changes
            if (Utilities.stringsDiffer(currMusicBasePath, newMusicBasePath))
            {
                UserSettings.removeVal(UserSettings.PREF_MUSIC_BASE_PATH);
                UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, newMusicBasePath);
                ret = true;
            }
            if (Utilities.stringsDiffer(currDatabasePath, newSharedDbBasePath))
            {
                UserSettings.removeVal(UserSettings.PREF_DATA_BASE_PATH);
                UserSettings.getAndPutString(UserSettings.PREF_DATA_BASE_PATH, newSharedDbBasePath);
                ret = true;
            }

            return ret;
        }


        /************************************************************************************
         *
         * Helper to reset the paths
         *
         ***********************************************************************************/
        private boolean resetPaths(Context context)
        {
            boolean ret = false;
            if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            {
                // get new default values
                String defaultMusicBasePath = Utilities.getDefaultMusicBasePath(context);
                String defaultSharedDbBasePath = Utilities.getDefaultSharedDbBasePath(context);

                // check if there are changes
                ret = changePaths(context, defaultMusicBasePath, defaultSharedDbBasePath);
            }
            return ret;
        }


        /************************************************************************************
         *
         * Update GUI on path change
         *
         ***********************************************************************************/
        private void updateGui()
        {
            /* unfortunately there is no way to update elements from stored preferences
            Preference pref = findPreference("prefMusicBasePath");
            String key = pref.getKey();
            pref.setKey(key);  // update!
            pref = findPreference("prefDataBasePath");
            key = pref.getKey();
            pref.setKey(key);  // update!
            */

            setPreferenceScreen(null);
            addPreferencesFromResource(R.xml.preferences);
            handleRestorePaths();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                handleSelectPath();
            }
        }


        /************************************************************************************
         *
         * Handle the "Restore Paths" button
         *
         ***********************************************************************************/
        private void handleRestorePaths()
        {
            Preference restoreDefaultPaths = findPreference("prefRestoreDefaultPaths");
            if (restoreDefaultPaths != null)
            {
                restoreDefaultPaths.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(@NonNull Preference preference)
                    {
                        FragmentActivity activity = getActivity();
                        if (activity != null)
                        {
                            if (resetPaths(activity))
                            {
                                Toast.makeText(activity, R.string.str_paths_updated, Toast.LENGTH_LONG).show();
                                updateGui();
                            }
                            else
                            {
                                Toast.makeText(activity, R.string.str_paths_unchanged, Toast.LENGTH_LONG).show();
                            }

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                });
            }
        }


        /************************************************************************************
         *
         * Handle the "Select Path" button
         *
         ***********************************************************************************/
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        private void handleSelectPath()
        {
            Preference prefSelectPath = findPreference("prefSelectPath");
            if (prefSelectPath != null)
            {
                prefSelectPath.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(@NonNull Preference preference)
                    {
                        FragmentActivity activity = getActivity();
                        if (activity != null)
                        {
                            startSafPickerForDocumentTree();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                });
            }
        }


        /************************************************************************************
         *
         * Fragment method
         *
         ***********************************************************************************/
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            registerOpenDocumentTreeCallback();
            setPreferencesFromResource(R.xml.preferences, rootKey);
            handleRestorePaths();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                handleSelectPath();
            }
        }
    }


    /************************************************************************************
     *
     * Activity method
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }
}
