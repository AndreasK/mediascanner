/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.mediascanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.preference.PreferenceManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * helper class for user settings (preferences)
 * @noinspection CommentedOutCode, RedundantSuppression
 */
@SuppressWarnings("WeakerAccess")
public class UserSettings
{
    public static final String dbDirectory = "ClassicalMusicDb";
    private static final String LOG_TAG = "UserSettings";

    public static final String PREF_MUSIC_BASE_PATH = "prefMusicBasePath";
    public static final String PREF_DATA_BASE_PATH = "prefDataBasePath";
    public static final String PREF_EXTRACT_FOLDER_IMAGES = "prefExtractFolderImages";
    public static final String PREF_MAX_SIZE_OF_FOLDER_IMAGES = "prefMaxSizeOfFolderImages";
    public static final String PREF_SCALE_EXISTING_FOLDER_IMAGES = "prefScaleExistingFolderImages";
    public static final String PREF_KEEP_BACKUP_OF_ORIGINAL_FOLDER_IMAGES = "prefKeepBackupOfOriginalFolderImages";
    public static final String PREF_SHOW_DEBUG_MENU_ENTRIES = "prefShowDebugMenuEntries";
    public static final String PREF_FILENAME_OF_ALBUM_IMAGES = "prefFileNameOfAlbumImages";
    public static final String PREF_RENAME_EXISTING_FOLDER_IMAGES = "prefRenameExistingFolderImages";
    // in fact unused, as there is no GUI element. Setting values will always be 'false'.
    public static final String PREF_DBG_REQUEST_MEDIA_FILE_ACCESS = "prefDebugRequestMediaFileAccess";
    public static final String PREF_IGNORE_NOMEDIA = "prefIgnoreNoMedia";

    private static SharedPreferences mSharedPrefs;

    @SuppressWarnings("WeakerAccess")
    public static class AppVersionInfo
    {
        String versionName = "";
        int versionCode = 0;
        String strCreationTime = "";
        boolean isDebug;
    }

    @SuppressWarnings("WeakerAccess")
    public static AppVersionInfo getVersionInfo(Context context)
    {
        AppVersionInfo ret = new AppVersionInfo();
        PackageInfo packageinfo = null;

        try
        {
            packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.d(LOG_TAG, "getVersionInfo() : " + e);
        }

        if (packageinfo != null)
        {
            ret.versionName = packageinfo.versionName;
            ret.versionCode = packageinfo.versionCode;
        }

        // get ISO8601 date instead of dumb US format (Z = time zone) ...
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        ret.strCreationTime = df.format(buildDate);
        ret.isDebug = BuildConfig.DEBUG;

        return ret;
    }


    public static void setContext(Context context)
    {
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setContextIfNecessary(Context context)
    {
        if (mSharedPrefs == null)
        {
            Log.w(LOG_TAG, "setContextIfNecessary() : was null");
            mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    /*
    * updates a preference value and returns previous value or defVal, if none
     */
    /*
    @SuppressLint("ApplySharedPref")
    static int updateValStoredAsString(final String key, int newVal, int defVal)
    {
        int prevVal = defVal;

        if (mSharedPrefs.contains(key))
        {
            prevVal = getIntStoredAsString(key, defVal);
        }

        if (prevVal != newVal)
        {
            String vds = Integer.toString(newVal);
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.commit();
        }

        return prevVal;
    }
    */

    /*
    * get a numerical value from the preferences and repair it, if necessary
     */
    public static int getIntStoredAsString(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        int v;
        try
        {
            String vs = mSharedPrefs.getString(key, vds);
            v = Integer.parseInt(vs);
        }
        catch(NumberFormatException e)
        {
            v = defaultVal;
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return v;
    }

    /*
    * put and commit
     */
    public static void putVal(final String key, int theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putInt(key, theVal);
        prefEditor.apply();
        //prefEditor.commit();    // make sure datum is written to flash now
    }

    /*
    * put and commit
     */
    /*
    public static void putVal(final String key, boolean theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putBoolean(key, theVal);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }
    */

    /*
    * get an integer value from the preferences
     */
    /*
    public static int getInt(final String key, int defaultVal)
    {
        return mSharedPrefs.getInt(key, defaultVal);
    }
    */

    /*
    * get a boolean value from the preferences
     */
    public static boolean getBool(final String key, boolean defaultVal)
    {
        return mSharedPrefs.getBoolean(key, defaultVal);
    }

    /*
     * get a text value from the preferences
     */
    public static String getString(final String key)
    {
        return mSharedPrefs.getString(key, null);
    }

    /*
    * get a text value from the preferences and repair it, if necessary
     */
    public static String getAndPutString(final String key, String defaultVal)
    {
        if (!mSharedPrefs.contains(key))
        {
            // not set, yet: write it
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, defaultVal);
            prefEditor.apply();
            return defaultVal;
        }
        return mSharedPrefs.getString(key, defaultVal);
    }

    /*
     * remove, but not commit
     */
    public static void removeVal(final String key)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.remove(key);
        prefEditor.apply();
    }

    /*
     * Application specific functions (TODO: move to derived class)
     */

    public static String getAlbumPicFname()
    {
        return getAndPutString(UserSettings.PREF_FILENAME_OF_ALBUM_IMAGES, "albumart");
    }

}
