/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MEDIASCANNER_NATIVE_LIB_TAGS_H
#define MEDIASCANNER_NATIVE_LIB_TAGS_H


extern void handleOggVorbis(Context *c);
extern void handleOggOpus(Context *c);

extern void handleMp3(Context *c);
extern void handleMp4(Context *c);
extern void handleFlac(Context *c);

extern void extractPictureMp3(Context *c, const char *audio_file_path, const char *destination_path);
extern void extractPictureMp4(Context *c, const char *audio_file_path, const char *destination_path);
extern void extractPictureFlac(Context *c, const char *audio_file_path, const char *destination_path);
extern void extractPictureOggVorbis(Context *c, const char *audio_file_path, const char *destination_path);
extern void extractPictureOggOpus(Context *c, const char *audio_file_path, const char *destination_path);

#endif //MEDIASCANNER_NATIVE_LIB_TAGS_H
