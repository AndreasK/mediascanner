/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MEDIASCANNER_NATIVE_CMD_H
#define MEDIASCANNER_NATIVE_CMD_H

// sync with Java!
#define cmdJniCreateDb               1
#define cmdJniQueryAudioFiles        2
#define cmdJniQueryAlbums            3
#define cmdJniCollectAlbums          4
#define cmdJniTruncateAudioFileTable 5
#define cmdJniTruncateAlbumTable     6
#define cmdJniRemoveTables           7
#define cmdJniGetNumAudioFiles       8
#define cmdJniGetNumAlbums           9
#define cmdJniWriteDbVersion         10
#define cmdJniGetDbVersion           11
#define cmdJniAutoScan               12
#define cmdJniCheckDbVersion         13
#define cmdJniUpdateAlbumPictures    14

// modes for scanDirectoryJNI()
#define cmdJniScanCountAllAudioFiles        0
#define cmdJniScanCollectAllAudioFiles      1
#define cmdJniScanCollectNewAudioFiles      2
#define cmdJniScanUpdateSpecifiedAudioFiles 3

#endif //MEDIASCANNER_NATIVE_CMD_H
